#!/bin/bash
# Link the hooks from git-flow-avh

set -o errexit
set -o nounset

BASEDIR=$(realpath "${0%/linkinPark.sh}")
echo "Creating hooks..."
HOOKDIR=${BASEDIR}/template/hooks

if [ ! -d "${HOOKDIR}" ]; then
    mkdir -p "${HOOKDIR}"
fi

# shellcheck disable=SC2045
for FILE in $(ls "${BASEDIR}/gitflow-avh/hooks"); do
    # Do not allow local "finish" hooks - merges to be done on Bitbucket
    if [ "${FILE%finish}" == "${FILE}" ] ; then
        cp "${BASEDIR}/gitflow-avh/hooks/${FILE}" "${HOOKDIR}/${FILE}";
        chmod a+x "${HOOKDIR}/${FILE}";
    fi
done

echo "Done"