module.exports = {
  env: {
    browser: true,
    es6: true,
  },
  extends: [
    'react-app',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: [
    'react',
    '@typescript-eslint',
  ],
  rules: {
    // Adding this rule mainly to use namespaces in extending jest matchers.
    "@typescript-eslint/no-namespace": "off",
    //  Adding this rule because Webstorm is giving tons of false positives and they're annoying.
    "no-unused-vars": "off"
  },
};
