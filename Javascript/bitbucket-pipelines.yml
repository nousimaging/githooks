# This pipeline allows you to build and push your docker image to a Docker Hub account.
# The workflow allows running tests, code linting and security scans on feature branches (as well as master).
# The docker image will be validated and pushed to the docker registry after the code is merged to master.

# Prerequisites: $DOCKERHUB_USERNAME, $DOCKERHUB_PASSWORD setup as deployment variables

image: atlassian/default-image:2

pipelines:
  default:
    - parallel:
      - step:
          name: Build and Test
          script:
            - IMAGE_NAME=$BITBUCKET_REPO_SLUG
            - docker build . --file Dockerfile --tag ${IMAGE_NAME}
            - docker run -a STDOUT -a STDERR -w /app ${IMAGE_NAME} yarn run test
          services:
            - docker
          caches:
            - docker
      - step:
          name: Lint the Dockerfile
          image: hadolint/hadolint:latest-debian
          script:
            - hadolint Dockerfile
      - step:
          name: Lint the code
          script:
            - IMAGE_NAME=$BITBUCKET_REPO_SLUG
            - docker build . --file Dockerfile --tag ${IMAGE_NAME}
            - docker run -a STDOUT -a STDERR -w /app ${IMAGE_NAME} yarn run lint
          services:
            - docker
          caches:
            - docker
      - step:
            name: Security Scan
            script:
              # Run a security scan for sensitive data.
              # See more security tools at 
              # https://bitbucket.org/product/features/pipelines/integrations?&category=security
              - pipe: atlassian/git-secrets-scan:0.4.3
  branches:
    main:
      - step:
          name: Build and Test
          script:
            - IMAGE_NAME=$BITBUCKET_REPO_SLUG
            - docker build . --file Dockerfile --tag ${IMAGE_NAME}
            - docker run -a STDOUT -a STDERR -w /app ${IMAGE_NAME} yarn run test
            - docker save ${IMAGE_NAME} --output "${IMAGE_NAME}.tar"
          services:
            - docker
          caches:
            - docker
          artifacts:
            - "*.tar"
      - step: 
          name: Push and Tag
          image: 
          script:
            - yarn version --new-version ${GIT_VERSION_TAG} --no-git-tag-version
          services:
            - docker
      - step:
          name: Deploy to Production
          deployment: Production
          image: python:3
          script:
            - echo ${DOCKERHUB_PASSWORD} | docker login --username "$DOCKERHUB_USERNAME" --password-stdin
            - IMAGE_NAME=$BITBUCKET_REPO_SLUG
            - docker load --input "${IMAGE_NAME}.tar"
            - VERSION="prod-$(yarn info version).${BITBUCKET_BUILD_NUMBER}"
            - IMAGE=${DOCKERHUB_NAMESPACE}/${IMAGE_NAME}
            - docker tag "${IMAGE_NAME}" "${IMAGE}:${VERSION}"
            - docker push "${IMAGE}:${VERSION}"
          services:
            - docker