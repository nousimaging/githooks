From pytest documentation:

> In the simplest terms, a test is meant to look at the result of a particular behavior, and make sure that result aligns with what you would expect. Behavior is not something that can be empirically measured, which is why writing tests can be challenging.
> 
> “Behavior” is the way in which some system acts in response to a particular situation and/or stimuli. But exactly how or why something is done is not quite as important as what was done.
> 
> You can think of a test as being broken down into four steps:
> 
>  1. Arrange
>  2. Act
>  3. Assert
>  4. Cleanup
>
> *Arrange* is where we prepare everything for our test. This means pretty much everything except for the “act”. It’s lining up the dominoes so that the act can do its thing in one, state-changing step. This can mean preparing objects, starting/killing services, entering records into a database, or even things like defining a URL to query, generating some credentials for a user that doesn’t exist yet, or just waiting for some process to finish.
> 
> *Act* is the singular, state-changing action that kicks off the behavior we want to test. This behavior is what carries out the changing of the state of the system under test (SUT), and it’s the resulting changed state that we can look at to make a judgement about the behavior. This typically takes the form of a function/method call.
> 
> *Assert* is where we look at that resulting state and check if it looks how we’d expect after the dust has settled. It’s where we gather evidence to say the behavior does or does not align with what we expect. The assert in our test is where we take that measurement/observation and apply our judgement to it. If something should be green, we’d say assert thing == "green".
> 
> *Cleanup* is where the test picks up after itself, so other tests aren’t being accidentally influenced by it.
> 
> At it’s core, the test is ultimately the act and assert steps, with the arrange step only providing the context. Behavior exists between act and assert.

Reliable tests have several requirements that ensure their validity and usefulness:

1. They are *isolated*, meaning that the initial test state is consistant and repeatable.  All tests should be able to be run singularly, in any random order, (or not at all!) and the results of all the other tests will not change.
2. They are *repeatable*, meaning that the outcome of the test is not dependent on any external state or side effect.  Tests are deterministic.
3. They are *readable*, meaning that the tests should serve as technical documentation for the code they evaluate.  In the case of business driven-development (BDD), tests can also serve to document user acceptance requirements.  Test naming conventions and docstrings should emphasize human readability and allow for references to relevant JIRA tickets.
4. They are *fast*, meaning that the contribution of any test should not slow the completion of the entire test bed appreciably.  Testing that is fast is run more often and is groomed more often than slow test cases.  Slow tests (> 30 secs each) should be examined to determine how to increase their efficiency or ways to parallelize their execution.

# Writing good tests
The first step in writing good tests is to _write testable code_.  When writing code before considering how to test it, it is very easy to create situations where retroactive test creation is very difficult or practically impossible!  Consider how to test a class method that requires other classes/objects that are not easily simulated (i.e. "mocked") as input.  Or code that depends on the internal state of an object from an imported package, like `pydicom` or `requests`.  There are many ways to "paint yourself into a corner" and realize that the work needed to test a section of code outweighs the effort needed to develop the code initially!  Writing testable code means that as a developer, you know how to test the code you create in a straightforward manner and you avoid architecture and patterns that are difficult to test.

The second step in writing good tests is to _always write a failing test_.  This may seem counter-intuitive but ensuring that you have written a test that fails means that a) your test is being identified by the test runner and b) it is being evaluated correctly by the test runner.  For example, suppose you have the code below and would like to write a test retrospectively:

```python
def my_function(a, b=1):
    return a*b
```

Let's write a passing test now:

```python
from . import my_function

def eval_my_function():
    assert my_function(5) == 5
```

The test is correct, except that the naming breaks the rule for pytest (must match "[Tt]est*" or "*[Tt]est")!  In a small test bed, you might notice that the test wasn't found but in a large test bed it would be easy to overlook if you're going quickly.  What if we wrote the assert to equal 4 instead?  It would be immediately obvious that there was something wrong with the test because _the test runner would not report a failure_!  Now consider a test that should fail (in our opinion) but isn't:

```python
from . import my_function

def test_my_function():
    """ my_function should never return a string """
    # Arrange
    value = '5'
    expected = value
    # Act
    actual = my_function(value)
    # Assert 
    assert str(expected) == actual
```

Here the author made the mistake of sending the string value '5' to `my_function` and the output would indeed be a string.  If we were not expecting a test failure, it would be understandable to assume the test was doing what we intended it to do...

## Test-driven Development

A solid way to consistantly write code with testing in mind is to follow the practice of "test-driven development" (TDD).  In TDD, developers follow a simple cycle of "red, green, refactor".  The first step, "red", is where you write _the simplest test that exercises the functionality you want to achieve_ and run it.  Because you have not implemented the code to make this test pass yet, your test will fail - hence "red".  In the "green" step, you now write the code fulfills the test requirements, making your previously failing test now pass.  This is typically a brute force or similarly unsophisticated solution whose sole requirement is test passage.  The third step, "refactor", is where you now clean up your code, ensuring any changes you make do not cause the underlying logic to change (as verified by a passing test at the conclusion).  These steps are repeated continuously until the specific feature/bugfix is complete.

