#!/bin/bash
set -o errexit
set -o nounset
set -o pipefail

clean_up() {
    rm -rf "${BASEDIR}/template"
}

python_setup() {
    # if [ ! -f pyproject.toml ] ; then
    #     echo "pyproject.toml file not found! Script must run in the project's top-level directory (see PEP 518)!"
    #     exit 2
    # fi
    CONFIGPATH=${BASEDIR}/Python/.pre-commit-config.yaml
    cp -R "${BASEDIR}"/Python/* "${BASEDIR}"/template/
}

js_setup() {
    # if [ ! -f package.json ] ; then
    #     echo "package.json not found! Script must run in the project's top-level directory!"
    #     exit 2
    # fi
    CONFIGPATH=${BASEDIR}/Javascript/.pre-commit-config.yaml
    cp -R "${BASEDIR}"/Javascript/* "${BASEDIR}"/template/
}

SCRIPT=$(realpath "$0")
BASEDIR=${SCRIPT%/setup.sh}  # supports "bash setup.sh" & "./setup.sh"

clean_up  # Clean up old directory
bash "${BASEDIR}"/linkinPark.sh  # Copy hooks
git config --global init.templateDir "${BASEDIR}"/template  # Setup the Git template directory

CONFIGPATH=""
# TODO: support multiple languages per repo (shell + JS, etc.)
echo "Choose the project language:"
echo
# echo "[B]ash"
# echo "[D]ocker"
echo "[J]avascript (Node)"
echo "[P]ython"
echo

# shellcheck disable=SC2162
read LANGUAGE

case "$LANGUAGE" in
    "J" | "j" ) js_setup ;;
    "P" | "p" ) python_setup ;;
    * ) echo "Unrecognized value '${LANGUAGE}'.  Exiting..." && clean_up && exit 1 ;;
esac

# If necessary, install pre-commit
if ! which pre-commit &> /dev/null ; then
    pip3 install pre-commit
fi

pre-commit init-templatedir --color=always -c "${CONFIGPATH}" template

echo "To bootstrap current projects with this language, go to that project's top-level directory (that contains .git/):

cp -Rn ${BASEDIR}/template/* .
pre-commit install -f --install-hooks
pre-commit run --all-files
"
exit 0