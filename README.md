This is a collection of (hopefully) useful git hooks that will help align us on the processes and patterns we use.

---
# Docs 

- Git flow: http://danielkummer.github.io/git-flow-cheatsheet/  (Note: all "finish" hooks are disabled.  Merges must be done in Bitbucket.)
- Shellcheck: https://github.com/koalaman/shellcheck
- Pre-commit: https://pre-commit.com/index.html
  
## Setup

### Visual Studio setup

*Note*: If you use Visual Studio Code and don't use the commandline for `git` actions, you can skip the "Commandline" section below.

1. Copy the `extensions.json` file to your Nous repos: `cp -n .vscode/extensions.json <top-level/path/to/repo>/.vscode/`
2. If this fails because the file already exists there, manually copy the relevant section to the destination file.  If it fails because `.vscode/` does not exist in the workspace, create it and try again.
3. Open the workspaces in VS Code and you will be prompted to install the recommended extensions.  Select "Install".

#### Git Flow Support

In order to interact with `git-flow` in VS Code, you will have to create branches via the *Command Prompt*.  (Currently, 'bugfix' is not supported, which is one reason we _highly recommend_ using the command line for branch management.)  If you are using the VS Code extension, you must run `GitFlow: Initialize GitFlow` via the *Command Prompt* to set up your extension for the current workspace.

### Commandline

In order to set up the hooks for your usage, you will need to use Git version 2.9 or greater.

1. Clone this repository in a location that makes the most sense to you.  As an example, we'll clone the repo in `~/source_code`: `$ cd ~/source_code; git clone <repoURL/githooks.git>`
2. In the repo, run `git submodule update --init` to populate the submodules.
3. Run `setup.sh`.  This will create the `template` directory, configure your Git template directory for the language type you choose, and allow you to bootstrap existing repos with the hooks you need.  
4. Note on previous step: if you need to change languages (for instance, to make a new project in JS after making one in Python), _you must run the `setup.sh` again and select the language for the new repo_.

#### Git Flow support

1. Install `git-flow-avh`.  See the installation instructions here: [https://github.com/petervanderdoes/gitflow-avh/wiki/Installation](https://github.com/petervanderdoes/gitflow-avh/wiki/Installation).
2. For each repository that will follow the git flow pattern, run `git flow init` in that repository's directory.

For more information on how to use `git flow`, see [https://github.com/petervanderdoes/gitflow-avh/wiki](https://github.com/petervanderdoes/gitflow-avh/wiki)

#### Commitizen support

TODO

#### pre-commit support

[https://pre-commit.com/index.html](https://pre-commit.com/index.html)

## Updating the submodules

All the hooks here are either homegrown or from other projects (`gitflow-avh`, etc.) and thus we need to update the symlinks and permissions when we do a `git submodule set-branch` or similar.  

TODO: We have implemented a hook in this submodule to do the relevant work for us.

---

## Conflicts and naming

To avoid naming collisions, we name all hooks `<parent_repo>_<hook>`.